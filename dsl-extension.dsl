job('Build Extension') {
  description('Builds utility jar for dynamic dsl creation')
  logRotator { 
    daysToKeep(-1)
    numToKeep(10)
    artifactDaysToKeep(-1)
    artifactNumToKeep(10)
  }

  scm {
    git{
      remote{
        url 'ssh://git@engstash.int.kronos.com:7999/JOBS/dsl-extension.git'
        credentials 'aa197130-352e-41e5-9a2b-0800200c9a66'
      }
      branch 'master'
      createTag = false
    }
  }
  triggers {
	  scm('H/15 * * * *')
  }
  
  wrappers{
    preBuildCleanup()
    timestamps()
    buildUserVars()
  }

  steps {
    gradle{
      tasks 'clean'
      tasks 'build'
      tasks 'publish'
      tasks 'groovydoc'
      useWrapper = false
    } 
  }
  publishers {
        publishHtml {
            report('build/docs/groovydoc/') {
                reportName('Groovydoc')
            }
          }
        }

   

}
