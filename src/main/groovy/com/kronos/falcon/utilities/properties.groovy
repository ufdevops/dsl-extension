package com.kronos.falcon.utilities

class KronosProperties {

  static Properties properties
  
  static void loadProperties(String props)
  {
	 properties = new Properties()
	 String[] keyValuePairs = props.split("\n");
	 for(String pair : keyValuePairs)
	 {
		int equal_ind = pair.indexOf("=")
		String key = pair.substring(0,equal_ind)
		String val = pair.substring(equal_ind+1)
		properties.put(key, val)
	 }
  }
  
  static String get(String key)
  {
	 return properties.getProperty(key)
  }

  public static void main(String[] args)
  {
	 KronosProperties.loadProperties("ARTIFACTORY_QUERY_URL=https://artifactory.falcon.kronos.com/artifactory/api/search/artifact?name=%1&repos=%2\nARTIFACTORY_SERVER=artifactory.falcon.kronos.com\nAWS_JENKINS_SERVER=ci.falcon.kronos.com")

	 System.out.println (KronosProperties.get("ARTIFACTORY_QUERY_URL"))
	 System.out.println (KronosProperties.get("ARTIFACTORY_SERVER"))
	 System.out.println (KronosProperties.get("AWS_JENKINS_SERVER"))
  }  
}
