package com.kronos.falcon.utilities

class JenkinsJob {
  static String currentPhase = 'Phase-5'
  static String GIT_URL_BASE = 'ssh://git@engstash.int.kronos.com:7999'
  static String GIT_CREDS = 'aa197130-352e-41e5-9a2b-0800200c9a66'
  static String providedGitProject = ''
  static String providedGitRepo =''

  static void addScm(def useJob, def useProject, def useRepo, def useBranch) {
    providedGitProject = useProject
    providedGitRepo = useRepo
    String gitUrl = [GIT_URL_BASE, useProject, useRepo].join('/') + '.git'

    useJob.with {
      scm {
        git{
          remote{
            name 'origin'
            url gitUrl
            credentials GIT_CREDS
          }
          branch useBranch
          createTag = false
        }
      }
    }
  }

  /**
   * Define a 'multiscm' block for the specified job with a list of Git repos
   * @param useJob job to add repos to
   * @param repos list of Git repos; each repo is a map with these required keys: project, repo, branch
   */
  static void addMultiScm(def useJob, Map... repos) {
    useJob.with {
      multiscm {
        repos.each { repo ->
          git {
            remote {
              name 'origin'
              url [GIT_URL_BASE, repo.project, repo.repo].join('/') + '.git'
              credentials GIT_CREDS
            }
            branch repo.branch
            relativeTargetDir repo.repo // keep repos from overwriting one another
            createTag = false
          }
        }
      }
    }
  }

  static void addScmTrigger(def useJob, scmFrequency = 'H/5 * * * *') {
    useJob.with {
      triggers {
        scm(scmFrequency)
      }
    }
  }

  static void addCronTrigger(def useJob, cronFrequency = 'H/15 * * * *') {
    useJob.with {
      triggers {
        cron(cronFrequency)
      }
    }
  }

  static void addBasics(def useJob, def blk=true, String tagName='') {
    useJob.with {
      configure { project ->
        (project / 'authToken').setValue('KRONITES')
      }
      configure { project ->
        project / publishers / 'com.chikli.hudson.plugin.naginator.NaginatorPublisher' {
          regexpForRerun '.*Slave went offline during the build|.*Invalid or unsupported zip format.*|.*Registry returned [0-9]{3} for GET.*|.*ENOENT: no such file or directory, open.*|.*ENOTEMPTY: directory not empty.*'
          rerunIfUnstable false
          rerunMatrixPart false
          checkRegexp true
          delay (class: 'com.chikli.hudson.plugin.naginator.ProgressiveDelay'){
            increment 30
            max 180
          }
          maxSchedule 5
        }
      }

      logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(10)
      }

      wrappers{
        preBuildCleanup()
        timestamps()
        injectPasswords()
        buildUserVars()
      }

      if(tagName.equalsIgnoreCase('')){
        //Don't tag at all, for now while testing
      }
      else{
        publishers{
          git{
            //Adding Git Tagging for Phases
            forcePush()
            pushOnlyIfSuccess()
            def phaseToTag
            if(tagName.equalsIgnoreCase('default')){ //If its the default tag, then use the current phase
              phaseToTag = currentPhase
            }
            else { //Otherwise use the tag passed in
              phaseToTag = tagName
            }
            tag('origin', phaseToTag) {
                create()
                update()
            }
          }
        }
      }
    }
  }

  static void addPublishMail(def useJob, addresses = 'austin.johnson@kronos.com') {
    useJob.with {
      publishers {
        mailer(addresses, false, false)
      }
    }
  }

  static void addPublishHtml(def useJob, rep = 'reports/', pubName = 'Plato Report', reportFile = 'index.html') {
    useJob.with {
      publishers {
        publishHtml {
          report (rep) {
            reportName(pubName)
            alwaysLinkToLastBuild(false)
            reportFiles(reportFile)
          }
        }
      }
    }
  }

  static void addShellWebNpmInstall(def useJob) {
    useJob.with {
      steps {
        shell("""NPM_REGISTRY=\${ARTIFACTORY_SERVER}/artifactory/api/npm/web-virtual/
                |npm config set cache \${WORKSPACE}/cache
                |npm config set registry \${NPM_REGISTRY}
				        |npm config set _auth YXJ0aWZhY3Rvcnl1cGxvYWRlcjpBUDhSVHRkU1RaWDh2VVBaZFY5UXR4V3BBV0Q=
                |npm config set always-auth true
                |npm config set email engFalconDevOps@kronos.com
                |npm config set strict-ssl false
                |npm config set phantomjs_cdnurl \${ARTIFACTORY_SERVER}/artifactory/web-virtual/
                |npm cache clean
                |npm install""".stripMargin())
        shell('sed -i "s#http://engart.int.kronos.com#https://artifactory.falcon.kronos.com#" ./config/artifactory.js*')
      }
    }
    //Will delete the cache for all web based jobs after running the build, which helps both disk space and npm errors.
    addDeleteCache(useJob)
  }

  static void addShellWebNpmInstallWithPublish(def useJob) {
    useJob.with {
      steps {
        shell("""NPM_REGISTRY=\${ARTIFACTORY_SERVER}/artifactory/api/npm/web-virtual/
                |npm config set cache \${WORKSPACE}/cache
                |npm config set registry \${NPM_REGISTRY}
                |npm config set _auth YXJ0aWZhY3Rvcnl1cGxvYWRlcjpBUDhSVHRkU1RaWDh2VVBaZFY5UXR4V3BBV0Q= -g
                |npm config set always-auth true -g
                |npm config set email superuser@kronos.com -g
                |npm config set strict-ssl false
                |npm config set phantomjs_cdnurl \${ARTIFACTORY_SERVER}/artifactory/web-virtual/
                |npm cache clean
                |npm install""".stripMargin())
        shell('sed -i "s#http://engart.int.kronos.com#https://artifactory.falcon.kronos.com#" ./config/artifactory.js*')
      }
    }
    //Will delete the cache for all web based jobs after running the build, which helps both disk space and npm errors.
    addDeleteCache(useJob)
  }

  static void addShellWebNpmInstallBranched(def useJob, def virtual, def repo) {
    useJob.with {
      steps {
        shell("""NPM_REGISTRY=\${ARTIFACTORY_SERVER}/artifactory/api/npm/${virtual}/
                |npm config set cache \${WORKSPACE}/cache
                |npm config set registry \${NPM_REGISTRY}
                |npm config set _auth YXJ0aWZhY3Rvcnl1cGxvYWRlcjpBUDhSVHRkU1RaWDh2VVBaZFY5UXR4V3BBV0Q=
                |npm config set always-auth true
                |npm config set email engFalconDevOps@kronos.com
                |npm config set strict-ssl false
                |npm config set phantomjs_cdnurl \${ARTIFACTORY_SERVER}/artifactory/${virtual}/
                |npm cache clean
                |npm install""".stripMargin())
        shell('sed -i "s#http://engart.int.kronos.com#https://artifactory.falcon.kronos.com#" ./config/artifactory.js*')
        shell("sed -i \"s#web-npm-local#${repo}#\" ./config/artifactory.js*")
      }
    }
    //Will delete the cache for all web based jobs after running the build, which helps both disk space and npm errors.
    addDeleteCache(useJob)
  }

  static void addDeleteCache(def useJob){
    useJob.with{
       //Post build cleanup of cache
       configure { node ->
        node / publishers / 'hudson.plugins.ws_cleanup.WsCleanup'{
          patterns{
            'hudson.plugins.ws_cleanup.Pattern'{
              pattern 'cache/**/*'
              type 'INCLUDE'
            }
          }
          deleteDirs 'true'
          skipWhenFailed 'false'
          cleanWhenSuccess 'true'
          cleanWhenUnstable 'true'
          cleanWhenFailure 'true'
          cleanWhenNotBuilt 'true'
          cleanWhenAborted 'true'
          notFailBuild 'true'
          cleanupMatrixParent 'false'
        }
      }
      //Pre build cleanup of everything
      configure { node ->
        node / buildWrappers / 'hudson.plugins.ws_cleanup.PreBuildCleanup'{
          deleteDirs 'true'
        }
      }
    }
  }

  static void addShellPresNpmInstall(def useJob) {
    useJob.with {
      steps {
        shell("""NPM_REGISTRY=\${ARTIFACTORY_SERVER}/artifactory/api/npm/pres-virtual/
                |npm config set cache \${WORKSPACE}/cache
                |npm config set registry \${NPM_REGISTRY}
                |npm config set strict-ssl false
                |npm install""".stripMargin())
        shell('sed -i "s#http://engart.int.kronos.com#https://artifactory.falcon.kronos.com#" ./config/artifactory.js*')
      }
    }
  }

  static void addShellPresNpmInstallBranched(def useJob, def virtual, def repo) {
    useJob.with {
      steps {
        shell("""NPM_REGISTRY=\${ARTIFACTORY_SERVER}/artifactory/api/npm/${virtual}/
                |npm config set cache \${WORKSPACE}/cache
                |npm config set registry \${NPM_REGISTRY}
                |npm config set strict-ssl false
                |npm install""".stripMargin())
        shell('sed -i "s#http://engart.int.kronos.com#https://artifactory.falcon.kronos.com#" ./config/artifactory.js*')
        shell("sed -i \"s#pres-npm-local#${repo}#\" ./config/artifactory.js*")
      }
    }
  }

  static void addShellWebGrunt(def useJob, pubType = 'snapshot') {
    useJob.with {
      steps {
        shell('grunt fetchCommonComponents:'+pubType+' --url=${ARTIFACTORY_SERVER} --repository=${WEB_REPO} --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD} && grunt build publish:'+pubType+' --url=${ARTIFACTORY_SERVER} --repository=${WEB_REPO} --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD}')
      }
    }
  }

  static void addShellWebGruntBranched(def useJob, pubType = 'snapshot', def repo) {
    useJob.with {
      steps {
        shell('grunt fetchCommonComponents:'+pubType+' --url=${ARTIFACTORY_SERVER} --repository=' + repo + ' --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD} && grunt build publish:'+pubType+' --url=${ARTIFACTORY_SERVER} --repository=' + repo + ' --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD}')
      }
    }
  }

  static void addShellWebGruntBlackduck(def useJob, pubType = 'snapshot') {
    useJob.with {
      steps {
        shell('grunt fetchCommonComponents:'+pubType+' --url=${ARTIFACTORY_SERVER} --repository=${WEB_REPO} --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD} && grunt build --url=${ARTIFACTORY_SERVER} --repository=${WEB_REPO} --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD}')
      }
    }
  }

  static void addShellPresGrunt(def useJob, pubType = 'snapshot') {
    useJob.with {
      steps {
        shell('grunt fetchDependencies --url=${ARTIFACTORY_SERVER} --repository=${PRES_REPO} --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD} && grunt build publish:'+pubType+' --url=${ARTIFACTORY_SERVER} --repository=${PRES_REPO} --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD}')
      }
    }
  }

  static void addShellPresGruntBranched(def useJob, pubType = 'snapshot', def repo) {
    useJob.with {
      steps {
        shell('grunt fetchDependencies --url=${ARTIFACTORY_SERVER} --repository=' + repo + ' --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD} && grunt build publish:'+pubType+' --url=${ARTIFACTORY_SERVER} --repository=' + repo + ' --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD}')
      }
    }
  }

  static void addShellPresGruntBlackduck(def useJob, pubType = 'snapshot') {
    useJob.with {
      steps {
        shell('grunt fetchDependencies --url=${ARTIFACTORY_SERVER} --repository=${PRES_REPO} --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD} && grunt build --url=${ARTIFACTORY_SERVER} --repository=${PRES_REPO} --username=${ARTIFACTORY_USER} --password=${ARTIFACTORY_PASSWORD}')
      }
    }
  }

  static void addShellCmd(def useJob, cmd = '') {
    useJob.with {
      steps {
        shell(cmd)
      }
    }
  }

  static void addGradleBlackduck(def useJob, the_branch, useTasks = 'build', rootDir = null) {
  // See Alain Cousineau or Christian Miszczak.
  // For Blackduck: don't add "artifactoryPublish" for the tasks; don't use "textFinder" in the publishers section.
    useJob.with {
      steps {
        gradle{
          makeExecutable = true
          tasks useTasks
          useWrapper true
          if(the_branch == 'develop') {
            switches '-g=${WORKSPACE} -PartifactoryUser=${ARTIFACTORY_USER} -PartifactoryPassword=${ARTIFACTORY_PASSWORD} -PbuildInfo=true -PartifactoryServer=artifactory.falcon.kronos.com -Pteam=integration'
          }
          else if(the_branch == 'depManifest') {
            switches '-g=${WORKSPACE} -PartifactoryUser=${ARTIFACTORY_RELEASE_USER} -PartifactoryPassword=${ARTIFACTORY_PASSWORD} -PbuildInfo=true -PartifactoryServer=artifactory.falcon.kronos.com -PisSnapshot=false -Pteam=integration'
          }
          else {
            switches '-g=${WORKSPACE} -PartifactoryUser=${ARTIFACTORY_USER} -PartifactoryPassword=${ARTIFACTORY_PASSWORD} -PbuildInfo=true -PartifactoryServer=artifactory.falcon.kronos.com -PisSnapshot=false -Pteam=integration'
          }
          if(rootDir != null && rootDir.toString().contains('WORKSPACE')){
            fromRootBuildScriptDir()
            rootBuildScriptDir(rootDir.toString())
          }
        }
      }
    }
  }

  static void addGradle(def useJob, the_branch, useTasks = 'build artifactoryPublish', rootDir = null, garb2 = '') {
    useJob.with {
      steps {
        gradle{
          makeExecutable = true
          tasks useTasks+' artifactoryPublish'
          useWrapper true
          if(the_branch == 'develop') {
            switches '-g=${WORKSPACE} -PartifactoryUser=${ARTIFACTORY_USER} -PartifactoryPassword=${ARTIFACTORY_PASSWORD} -PbuildInfo=true -PartifactoryServer=artifactory.falcon.kronos.com' + " -PprovGitProj=${providedGitProject} -PprovGitRepo=${providedGitRepo}"
          }
          else if(the_branch == 'depManifest') {
            switches '-g=${WORKSPACE} -PartifactoryUser=${ARTIFACTORY_RELEASE_USER} -PartifactoryPassword=${ARTIFACTORY_PASSWORD} -PbuildInfo=true -PartifactoryServer=artifactory.falcon.kronos.com -PisSnapshot=false' + " -PprovGitProj=${providedGitProject} -PprovGitRepo=${providedGitRepo}"
          }
          else {
            switches '-g=${WORKSPACE} -PartifactoryUser=${ARTIFACTORY_USER} -PartifactoryPassword=${ARTIFACTORY_PASSWORD} -PbuildInfo=true -PartifactoryServer=artifactory.falcon.kronos.com -PisSnapshot=false' + " -PprovGitProj=${providedGitProject} -PprovGitRepo=${providedGitRepo}"
          }
          //if(rootDir != null && rootDir.toString().contains('WORKSPACE')){
          //  fromRootBuildScriptDir()
          //  rootBuildScriptDir(rootDir.toString())
          //}
        }
      }
      //Add in a default text finder to set jobs to yellow if they've already been published with the same version
      publishers {
        textFinder(/Please Update Your Version File To Publish/, '', true, false, true)
      }
    }
  }

  static void addGradleWithSubfolder(def useJob, the_branch, useTasks = 'build artifactoryPublish', rootDir = null) {
    useJob.with {
      steps {
        gradle{
          makeExecutable = true
          tasks useTasks+' artifactoryPublish'
          useWrapper true
          if(the_branch == 'develop') {
            switches '-g=${WORKSPACE} -PartifactoryUser=${ARTIFACTORY_USER} -PartifactoryPassword=${ARTIFACTORY_PASSWORD} -PbuildInfo=true -PartifactoryServer=artifactory.falcon.kronos.com' + " -PprovGitProj=${providedGitProject} -PprovGitRepo=${providedGitRepo}"
          }
          else if(the_branch == 'depManifest') {
            switches '-g=${WORKSPACE} -PartifactoryUser=${ARTIFACTORY_RELEASE_USER} -PartifactoryPassword=${ARTIFACTORY_PASSWORD} -PbuildInfo=true -PartifactoryServer=artifactory.falcon.kronos.com -PisSnapshot=false' + " -PprovGitProj=${providedGitProject} -PprovGitRepo=${providedGitRepo}"
          }
          else {
            switches '-g=${WORKSPACE} -PartifactoryUser=${ARTIFACTORY_USER} -PartifactoryPassword=${ARTIFACTORY_PASSWORD} -PbuildInfo=true -PartifactoryServer=artifactory.falcon.kronos.com -PisSnapshot=false' + " -PprovGitProj=${providedGitProject} -PprovGitRepo=${providedGitRepo}"
          }
          if(rootDir != null && rootDir.toString().contains('WORKSPACE')){
            fromRootBuildScriptDir()
            rootBuildScriptDir(rootDir.toString())
          }
        }
      }
      //Add in a default text finder to set jobs to yellow if they've already been published with the same version
      publishers {
        textFinder(/Please Update Your Version File To Publish/, '', true, false, true)
      }
    }
  }

  static void addDownstream(def useJob, job_name) {
    useJob.with {
      steps {
        downstreamParameterized {
          trigger(job_name, 'ALWAYS', true)
        }
      }
    }
  }

  static void addCodeCenter(def useJob, appName = '', versionNumber = '1.0', publishRepo = '', thirdPartyRepo = '', boolean blackduckEnabled = false, artifactoryAppliance = '-1522767978@1449194471144'){
      useJob.with{

        configure { project ->
          project / 'buildWrappers' / 'org.jfrog.hudson.gradle.ArtifactoryGradleConfigurator' {
                deployMaven true
                deployIvy true
                deployBuildInfo true
                includeEnvVars false
                runChecks false
                violationRecipients
                includePublishArtifacts false
                scopes
                licenseAutoDiscovery true
                disableLicenseAutoDiscovery false
                ivyPattern '[organisation]/[module]/ivy-[revision].xml'
                enableIssueTrackerIntegration false
                aggregateBuildIssues false
                artifactPattern '[organisation]/[module]/[revision]/[artifact]-[revision](-[classifier]).[ext]'
              nonM2Compatible false
                  artifactDeploymentPatterns{
                  includePatterns
                  excludePatterns
                  }
                discardOldBuilds true
                discardBuildArtifacts true
                passIdentifiedDownstream false
                matrixParams
                skipInjectInitScript true
                allowPromotionOfNonStagedBuilds false
                allowBintrayPushOfNonStageBuilds false
                blackDuckRunChecks blackduckEnabled
                blackDuckAppName "${appName}"
                blackDuckAppVersion "${versionNumber}"
                filterExcludedArtifactsFromBuild false
              details{
                  artifactoryName "${artifactoryAppliance}"
                  artifactoryUrl 'https://artifactory.falcon.kronos.com/artifactory'
                  deployReleaseRepository{
                      keyFromText "${publishRepo}"
                      keyFromSelect "${publishRepo}"
                    }
                  resolveReleaseRepository{
                      keyFromText "${thirdPartyRepo}"
                      keyFromSelect "${thirdPartyRepo}"
                      dynamicMode false
                    }
                  stagingPlugin{
                    pluginName 'None'
                    }
                  userPluginKey 'None'
                }
              deployArtifacts true
              envVarsPatterns{
                    includePatterns
                    excludePatterns '*password*,*secret*'
              }
              blackDuckReportRecipients 'engFalconDevOps@kronos.com'
              blackDuckIncludePublishedArtifacts false
              autoCreateMissingComponentRequests true
              autoDiscardStaleComponentRequests true

        }
      }
    }
  }

  static void addProtex(def useJob, String pName = '${JOB_NAME}', boolean blackduckEnabled = false){
    if(blackduckEnabled){
      useJob.with{
         configure { node ->
          node / publishers / 'com.blackducksoftware.integration.protex.jenkins.PostBuildProtexScan' {
            protexServerId '888fa899-c639-4a01-8157-d53b8ca591b2'
            protexPostCredentials '66b6665c-bcbf-4db3-a4b2-4e1632b70666'
            protexPostTemplateProjectName ''
            protexPostProjectName pName
            protexPostProjectSourcePath 'components/'
          }
        }
      }
    }
  }

  static void addProtexCustom(def useJob, String pName = '${JOB_NAME}', String pathToScan){
    useJob.with{
       configure { node ->
        node / publishers / 'com.blackducksoftware.integration.protex.jenkins.PostBuildProtexScan' {
          protexServerId '888fa899-c639-4a01-8157-d53b8ca591b2'
          protexPostCredentials '66b6665c-bcbf-4db3-a4b2-4e1632b70666'
          protexPostTemplateProjectName ''
          protexPostProjectName pName
          protexPostProjectSourcePath pathToScan
        }
      }
    }
  }

  static void addProtexFrontEnd(def useJob, String pName = '${JOB_NAME}', boolean blackduckEnabled = false){
    if(blackduckEnabled){
      useJob.with{
         configure { node ->
          node / publishers / 'com.blackducksoftware.integration.protex.jenkins.PostBuildProtexScan' {
            protexServerId '888fa899-c639-4a01-8157-d53b8ca591b2'
            protexPostCredentials '66b6665c-bcbf-4db3-a4b2-4e1632b70666'
            protexPostTemplateProjectName ''
            protexPostProjectName pName
            if(pName.contains('pres')){
              def sourcePath = pName.substring(pName.indexOf('-')+1)
              protexPostProjectSourcePath "${sourcePath}/"
            }
            else{
              protexPostProjectSourcePath 'apps/'
            }
          }
        }
      }
    }
  }

  static void addSonar(def useJob, pName = '', ver = '1.0', srcs = '') {

//    useJob.with {
//      configure { node ->
//        node / builders / 'hudson.plugins.sonar.SonarRunnerBuilder' {
//          properties """sonar.projectKey=com.kronos.${pName}
//                       |sonar.projectName=${pName}
//                       |sonar.projectVersion=${ver}
//                       |sonar.projectDescription='New CI Pipeline'
//                       |sonar.sources=${srcs}
//                       |sonar.language=java
//                       |sonar.java.source=1.8
//                       |sonar.binaries=./components/
//                       |sonar.junit.reportsPath=./build/test-results
//                       |sonar.jacoco.itReportPath=./build/jacoco/test.exec
//                       |sonar.jacoco.reportPath=./build/jacoco/test.exec""".stripMargin()
//        }
//      }
//    }

  }

  static void addArtifactory(def useJob, repository = 'falcon-deploy', pattern = '') {
    useJob.with {
      configure { node ->
        node / 'buildWrappers' << 'org.jfrog.hudson.generic.ArtifactoryGenericConfigurator' {
          details {
            artifactoryName '-1522767978@1449194471144'
            artifactoryUrl 'https://artifactory.falcon.kronos.com/artifactory'
            deployReleaseRepository {
              keyFromText repository
              dynamicMode 'true'
            }
          }
          deployPattern pattern
          deployBuildInfo 'true'
          includeEnvVars 'false'
          envVarsPatterns {
            excludePatterns '*password*,*secret*'
          }
          discardOldBuilds 'true'
          discardBuildArtifacts 'true'
          multiConfProject 'false'
        }
      }
    }
  }
  static void addJUnit(def useJob, int timeMarginValue=3000){
    useJob.with {
      publishers {
        archiveXUnit {
          jUnit{
            pattern  'build/test-results/*.xml'
            skipNoTestFiles false
            failIfNotNew true
          }
          skippedThresholds{
            unstable(100000)
            unstableNew(100000)
            failure(100000)
            failureNew(100000)
          }
          timeMargin(timeMarginValue)
        }
      }
    }
  }
  //
  // STARTING GARBAGE
  //
  static void addPublishJunit(def useJob, testPatern = 'test-results.xml') { //This is a mess but I'm not fixing 100 dsl files
    useJob.with {
      //publishers {
      //  archiveXUnit {
      //    jUnit {
      //      pattern testPatern
      //      skipNoTestFiles false
      //      failIfNotNew false
      //      deleteOutputFiles true
      //      stopProcessingIfError true
      //    }
      //  }
      //}
    }
  }

  static void addPublishJunitTest(def useJob, testPatern = 'build/**/test-results/*.xml') {
    useJob.with {
      publishers {
        archiveJunit(testPatern)
      }
    }
  }
//}

// Al Test

  static void aladdBasics(def useJob, def blk=true,  String tagName='') {
    useJob.with {
      configure { project ->
        (project / 'authToken').setValue('KRONITES')
      }
      configure { project ->
        project / publishers / 'com.chikli.hudson.plugin.naginator.NaginatorPublisher' {
          regexpForRerun '.*Slave went offline during the build'
          rerunIfUnstable false
          rerunMatrixPart false
          checkRegexp true
          delay (class: 'com.chikli.hudson.plugin.naginator.ProgressiveDelay'){
            increment 30
            max 180
          }
          maxSchedule 0
        }
      }

      logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(10)
      }

      wrappers{
        preBuildCleanup()
        timestamps()
        injectPasswords()
        buildUserVars()
      }



     }


   //  Add Git Publisher for Tagging of master branch objects
  //  if (jobName.contains("_manifest")) {
      useJob.with {
        configure { node ->
                node / publishers / 'hudson.plugins.git.GitPublisher'{
                        configVersion '2'
                        pushMerge 'false'
                        pushOnlyIfSuccess 'true'
                        forcePush 'false'
                        tagsToPush {
                               'hudson.plugins.git.GitPublisher_-TagToPush'{
                                        targetRepoName 'origin'
                                        tagName  'asdf'
                                        createTag 'true'
                              }
                        }
                 }
             }
         } // useJob
     //}  if (job.Name.contains("_manifest etc.

  } //end of aladdBasics




} // End of class
